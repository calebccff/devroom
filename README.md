# FOSDEM 2023

## Update

The submission deadline has been extended to 2022-12-12 (Monday)!

## FOSS on mobile devices devroom

We are happy to announce the physical FOSS on mobile devices devroom at FOSDEM 23!

This devroom is about everything related to Linux and Free Software on Linux-first 
based smartphones (such as the Librem 5 or the PinePhone) and other mobile devices 
running a regular (non-Android) Linux kernel. The topics can range from the 
very first bytes the bootloader runs, up to the 1 pixel offset you're fixing 
in your favorite toolkit. If you have been hacking, designing or reverse 
engineering in this field we would love to hear what you have been up to!


## Call for Participation

We are excited to invite the great communities around different projects
to submit their talks.

We are interested in seeing your demos, hearing your presentations and engaging
in lively discussions with developers and users.

Please bear in mind that all talks will be given in person this year.

If you're unsure if your proposed talk will be a good fit or if you have
other questions, feel free to reach out to us on matrix at #mobile-devroom:fosdem.org
or send a mail to mobile-devroom-manager@fosdem.org


### Important dates

- 12 December - Submission deadline
- 15 December - Announcement of selected talks
- 04 February 2023 - Devroom taking place (first half of Saturday: 10:30 - 14:30)

All deadline times are 23:59 UTC.


## Topics

Some topics of interest including, but not limited to:

- Distributions for mobile devices
- Mainline device support
- Building hardware for mobile FOSS OS's
- Mobile software ecosystem (GNOME, Plasma Mobile, etc)
- Specific software projects


## Submission details

Please use the following URL to submit a talk to FOSDEM 2023:
https://penta.fosdem.org/submission/FOSDEM23

- If you already have a Pentabarf account, please **don't** create a new one.
- Once logged in, select "Create Event" and click on "Show All" in the top right corner to display the full form.
- Uploading a photograph and writing a short biography is encouraged
- Please don't submit your talk to multiple tracks, instead choose
  the most appropriate one and/or reach out to us
- Add relevant information such as 
  title, subtitle, duration and short abstract 
  all of which will be visible in the FOSDEM schedule
  We're looking forward to your submissions!

## Licensing

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/)
.
